'use strict';

import path from 'path';

export default function (gulp, plugins, args, config, taskTarget, browserSync) {
  let dirs = config.directories;
  let dest = path.join(taskTarget);

  //Copy icons
  gulp.task('copy.icons', () => {
    return gulp.src([path.join(dirs.source, dirs.icons, '**/*')])
      .pipe(gulp.dest(path.join(dest, '/assets/fonts')));
  });

  //Copy styles
  gulp.task('copy.styles', () => {
    return gulp.src([path.join(dirs.source, dirs.styles, '**/*.css')])
      .pipe(gulp.dest(path.join(dest, '/assets/styles')));
  });

  //Copy scripts
  gulp.task('copy.scripts', () => {
    return gulp.src([path.join(dirs.source, dirs.scripts, '**/*.js'), '!' + path.join(dirs.source, dirs.scripts, '**/main.js')])
      .pipe(gulp.dest(path.join(dest, '/assets/scripts')));
  });

  //Copy api
  gulp.task('copy.api', () => {
    return gulp.src([path.join(dirs.source, dirs.api, '**/*.json')])
      .pipe(gulp.dest(path.join(dest, '/assets/api')));
  });

  // Copy
  gulp.task('copy', ['copy.styles', 'copy.scripts', 'copy.icons', 'copy.api'], () => {
    return gulp.src([
        path.join(dirs.source, '**/*'),
        '!' + path.join(dirs.source, '{**/\_*,**/\_*/**}')
        ,
        '!' + path.join(dirs.source, '**/*.pug')
        
      ])
      .pipe(plugins.changed(dest))
      .pipe(gulp.dest(dest));
  });
}
