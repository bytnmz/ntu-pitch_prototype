'use strict';

import $ from 'jquery';
import enquire from 'enquire.js';

export default class SiteHeader {
	constructor() {
		this.name = 'site-header';
		console.log('%s module', this.name.toLowerCase());
	}
}
