// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';

import $ from 'jquery';
import 'jquery-match-height';
import objectFitImages from 'object-fit-images';

import TableResponsive from '../_modules/utils/table-responsive/table-responsive';
import SelectClone from '../_modules/utils/select-clone/select-clone';
import BackToTop from '../_modules/utils/back-to-top/back-to-top';

import SiteHeader from '../_modules/organisms/site-header/site-header';


$(() => {
	//Polyfill for object-fit
	objectFitImages();

	// Apply wrapper for table
	if ($('table').length) {
		new TableResponsive();
	}

	new SiteHeader();
	new BackToTop();
	new SelectClone();

	$('.match-height').matchHeight();
});
